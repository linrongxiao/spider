package com.hymake;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hymake.dao.ClazzRepository;
import com.hymake.processor.ClazzProcessor;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpiderApplicationTests {
	
	@Autowired
	private ClazzProcessor clazzSpider;
	@Autowired
	private ClazzRepository clazzRepository;

	@Ignore
	@Test
	public void crawlClazz() {
		clazzRepository.deleteAll();
		clazzSpider.crawl(clazzSpider);
	}

}
