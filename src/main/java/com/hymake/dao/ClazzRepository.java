package com.hymake.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.hymake.model.Clazz;

@RepositoryRestResource(path = "clazz")
public interface ClazzRepository extends JpaRepository<Clazz, Integer> {

}
