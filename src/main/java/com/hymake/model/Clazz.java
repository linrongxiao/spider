package com.hymake.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

/**
 * 开班信息
 * 
 * @author lrx
 *
 */
@Entity
@Data
public class Clazz {
	@Id
	@GeneratedValue
	private Integer id;
	private String title;
	private String publishTime;
	@Column(length = 1000)
	private String url;

}
