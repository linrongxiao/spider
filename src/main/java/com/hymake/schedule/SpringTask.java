package com.hymake.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hymake.dao.ClazzRepository;
import com.hymake.processor.ClazzProcessor;

//    <!-- 定时器开关 -->
//    @EnableScheduling
//    Spring定时任务cronExpression的值（配置定时时间）格式说明：
//    一个cronExpression表达式有至少6个（也可能是7个）由空格分隔的时间元素。从左至右，这些元素的定义如下：
//    (1) 秒(0–59) ,  - * /   
//    (2)分钟(0–59), - * /   
//    (3)小时(0–23) , - * /   
//    (4)月份中的日期(1–31) , - * ? / L W C   
//    (5)月份(1–12或JAN–DEC) , - * /   
//    (6)星期中的日期(1–7或SUN–SAT) , - * ? / L C #   
//    (7)年份(1970–2099) (可选) 留空, - * /   
//    表达式意义 ： 
//    "0 0 12 * * ?" 每天中午12点触发   
//    "0 30 9 ? * *" 每天上午9:30触发   
//    "0 30 9 * * ? 2008" 2008年的每天上午9:30触发   
//    "0 * 13 * * ?" 在每天下午1点到下午1:59期间的每1分钟触发   
//    "0 0/5 13 * * ?" 在每天下午1点到下午1:55期间的每5分钟触发   
//    "0 0/5 13,18 * * ?" 在每天下午1点到2:55期间和下午6点到6:55期间的每5分钟触发   
//    "0 0-5 12 * * ?" 在每天下午1点到下午1:05期间的每1分钟触发   
//    "0 10,50 14 ? 3 WED" 每年三月的星期三的下午2:10和2:50触发   
//    "0 30 10 ? * MON-FRI" 周一至周五的上午10:30触发   
//    "0 30 10 15 * ?" 每月15日上午10:30触发   
//    "0 30 10 L * ?" 每月最后一日的上午10:30触发   
//    "0 30 10 ? * 6L" 每月的最后一个星期五上午10:30触发   
//    "0 30 10 ? * 6L 2006-2008" 2006年至2008年的每月的最后一个星期五上午10:30触发   
//    "0 15 9 ? * 6#3" 每月的第三个星期五上午9:15触发   
//    "0 5 * * * "   每天早上5点触发  
//    "0 */2 * * * "  每两小时触发  
//    "0 23-6/2,7 * * * " 晚上11点到早上6点之间每两个小时，早上7点触发  
//    "0 5 1 * 1-3"每个月的4号和每个礼拜的礼拜一到礼拜三的早上5点触发
//    "0 2 1 1 * " 1月1日凌晨2点触发
//    「?」字符：表示不确定的值
//    「,」字符：指定数个值
//    「-」字符：指定一个值的范围
//    「/」字符：指定一个值的增加幅度。n/m 表示从 n 开始，每次增加 m
//    「L」字符：用在日表示一个月中的最后一天，用在周表示该月最后一个星期 X
//    「W」字符：指定离给定日期最近的工作日 (周一到周五)
//    「#」字符：表示该月第几个周 X。6#3 表示该月第 3 个周五
@Component
public class SpringTask {

	private static final Logger log = LoggerFactory.getLogger(SpringTask.class);
	
	@Autowired
	private ClazzProcessor spider;
	@Autowired
	private ClazzRepository clazzRepository;

	/** 每天上午8点 
	 * @throws Exception */
	@Scheduled(cron = "0 8 11 * * ?") 
	public void craw() throws Exception {
		log.info("===================start spider==============");
		clazzRepository.deleteAll();
		spider.crawl(spider);
	}
	
	/** 相当Listener，程序启动执行一次  */
	@Scheduled(initialDelay = 1*1000,fixedRate=Long.MAX_VALUE)
	public void initCraw() throws Exception {
		log.info("===================start spider==============");
		clazzRepository.deleteAll();
		spider.crawl(spider);
	}

}
