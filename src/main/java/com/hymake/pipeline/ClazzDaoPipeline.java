package com.hymake.pipeline;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hymake.dao.ClazzRepository;
import com.hymake.model.Clazz;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

@Component
public class ClazzDaoPipeline implements Pipeline {

	@Autowired
	private ClazzRepository clazzDao;

	public void process(ResultItems resultItems, Task task) {
		List<Clazz> clazzs = resultItems.get("repo");
		if(clazzs != null && clazzs.size() > 0) {
			for (Clazz clazz : clazzs) {
				clazzDao.save(clazz);
			}
		}
	}
}
